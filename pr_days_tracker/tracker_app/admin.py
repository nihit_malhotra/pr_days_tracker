from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import User
from .models import TravelData, TravelHistory, UserCountryData, PRNotifications


# Register your models here.


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


# Admin page display settings
class TravelDataAdmin(admin.ModelAdmin):
    # columns to display
    list_display = ['user', 'country_name', 'exit_date', 'exit_date', 'number_of_days']

    # define search columns list, then a search box will be added at the top of list page.
    search_fields = ['user', 'country_name']

    # define filter columns list, then a filter widget will be shown at right side of list page.
    # list_filter = ['user', 'country_name']

    # define which field will be pre populated.
    # prepopulated_fields = {'dept_name': ('dept_name',)}

    # define model data list ordering.
    ordering = ('user', 'country_name', 'exit_date', 'exit_date', 'number_of_days')


class PRNotificationsAdmin(admin.ModelAdmin):
    # columns to display
    list_display = ['user', 'notification_type', 'email_date', 'email_sent', 'email_deleted']


admin.site.register(TravelData, TravelDataAdmin)
admin.site.register(TravelHistory)
admin.site.register(UserCountryData)
admin.site.register(PRNotifications, PRNotificationsAdmin)

