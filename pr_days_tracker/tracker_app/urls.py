from django.urls import path, include
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.home_view, name='home'),
    path('signup/', views.signup_view, name="signup"),
    path('otp/', views.otp_view, name="request_otp"),
    path('profile/', views.profile_view, name="profile"),
    path('dashboard/', views.dashboard_view, name="dashboard"),
    path('deactivate/', views.deactivate_account, name="deactivate"),
    path('travel/', views.travel_date_view, name="travel"),
    path('how/', views.how_to_view, name="how"),
    path('autonotify/', views.send_email_notifications, name="notify"),
    url(r'^profile/*', views.profile_view, name="profile_action"),
    url(r'^travel/(?P<form_action>[a-z]+)/(?P<row_id>[0-9]+)', views.travel_date_view, name="travel_action"),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
]

# Add Django site authentication urls (for login, logout, password management)
urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]
