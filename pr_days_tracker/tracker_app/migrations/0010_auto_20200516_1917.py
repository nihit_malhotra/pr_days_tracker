# Generated by Django 2.2 on 2020-05-17 02:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tracker_app', '0009_auto_20200516_1856'),
    ]

    operations = [
        migrations.RenameField(
            model_name='traveldata',
            old_name='wrt_country',
            new_name='country_name',
        ),
        migrations.AlterUniqueTogether(
            name='traveldata',
            unique_together={('country_name', 'user', 'entry_date')},
        ),
    ]
