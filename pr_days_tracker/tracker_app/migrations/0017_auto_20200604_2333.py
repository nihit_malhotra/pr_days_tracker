# Generated by Django 2.2 on 2020-06-05 06:33

from django.db import migrations, models
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('tracker_app', '0016_auto_20200604_1246'),
    ]

    operations = [
        migrations.AddField(
            model_name='traveldata',
            name='destination_country',
            field=django_countries.fields.CountryField(blank=True, max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='traveldata',
            name='reason',
            field=models.TextField(blank=True, null=True),
        ),
    ]
