# Generated by Django 2.2 on 2020-05-31 20:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tracker_app', '0014_auto_20200530_2145'),
    ]

    operations = [
        migrations.AddField(
            model_name='prnotifications',
            name='email_deleted',
            field=models.BooleanField(default=False),
        ),
    ]
