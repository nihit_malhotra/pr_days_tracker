from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .forms import CustomUserCreationForm, UserCountryForm, UserBasicData, TravelDatesForm
from django.core.mail import send_mail
from pr_days_tracker.settings import EMAIL_HOST_USER, EMAIL_ADMIN, PR_EMAIL_ADMIN, SCRIPT_VERSION
from django.template.loader import render_to_string
from .token_gen import account_activation_token
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.sites.shortcuts import get_current_site
from .models import User, UserCountryData, TravelData, PRNotifications
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django_countries.data import COUNTRIES
from datetime import datetime, timedelta
from django.db.models import Sum
import math
from django.http import HttpResponse, HttpResponseRedirect
from django_otp.decorators import otp_required
from django_otp.forms import OTPTokenForm
from django_otp.plugins.otp_email.models import EmailDevice
from django_otp.models import Device, DeviceManager
from django.utils import timezone

# from django.urls import reverse_lazy
# from django.views import generic
# Create your views here.


def home_view(request):
    send_email_notifications(request)
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    if request.user.is_authenticated:
        return_value = basic_profile(request)
        if return_value == 0:
            return redirect('/profile')
        else:
            return redirect('/dashboard')
    return render(request, 'home.html', {'context': context})


# class HomePageView(TemplateView):
# template_name = 'home.html'


# class Signup(generic.CreateView):
#     form_class = CustomUserCreationForm
#     success_url = reverse_lazy('login')
#     template_name = 'signup.html'

def signup_view(request):
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            current_site = get_current_site(request)
            # Activation email to new user
            mail_subject = "Account Activation Link - prdt.nihit.in"
            mail_body = render_to_string('email/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'protocol': request.scheme,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            send_mail(mail_subject, mail_body, EMAIL_HOST_USER, [user.email], fail_silently=False)
            # email to admin on new account creation
            mail_subject_admin = "NEW USER - prdt.nihit.in"
            mail_body_admin = mail_body = render_to_string('email/new_user_email.html', {
                'user': user,
                'domain': current_site.domain,
                'protocol': request.scheme,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            send_mail(mail_subject_admin, mail_body_admin, EMAIL_HOST_USER, [PR_EMAIL_ADMIN], fail_silently=False)
            return render(request, 'activation_sent.html')
    else:
        form = CustomUserCreationForm()
    return render(request, 'signup.html', {'form': form, 'context': context})


def activate(request, uidb64, token):
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        message = 'Thank you for your email confirmation. Now you can login your account.'
        return render(request, 'activated.html', {'message': message, 'context': context})
    else:
        message = 'Activation link is invalid!'
        return render(request, 'activated.html', {'message': message, 'context': context})


# view to display brief about the App
def how_to_view(request):
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    if request.user.is_authenticated:
        return_value = basic_profile(request)
        if return_value == 0:
            return redirect('/profile')
    return render(request, 'how.html', {'context': context})


@login_required
@otp_required(login_url='/otp/')
def profile_view(request):
    # keeping disabled on to restrict to CANADA for the moment
    disabled = "disabled"
    message = {}
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION,
        'notifications_true': 'checked',
        'notifications_false': ""
    }
    current_user = request.user
    try:
        # Check if user data exists
        user_object = UserCountryData.objects.get(user=current_user)
        disabled = "disabled"
    except ObjectDoesNotExist:
        user_object = ""
        message['gen'] = "All fields are required."
        context['notifications_true'] = "checked"
    if request.method == 'POST':
        message = {}
        user_country_data_form = UserCountryForm(request.POST)
        user_personal_data_form = UserBasicData(request.POST, instance=request.user)
        if user_country_data_form.is_valid():
            if user_object:
                user_object.entry_date = user_country_data_form.cleaned_data['entry_date']
                user_object.pr_date = user_country_data_form.cleaned_data['pr_date']
                # user_object.country_name = user_country_data_form.cleaned_data['country_name']
                print(user_country_data_form.cleaned_data['notifications_on'])
                user_object.notifications_on = user_country_data_form.cleaned_data['notifications_on']
                user_object.save()
                message['success'] = "Information Updated!!"
                update_final_notifications(request)
            else:
                c_data = user_country_data_form.save(commit=False)
                c_data.user = current_user
                c_data.save()
                message['success'] = "Information Saved!!"
                user_object = UserCountryData.objects.get(user=current_user)
                update_final_notifications(request)
                set_next_reminder(request)
        else:
            message = user_country_data_form.errors
        if user_personal_data_form.is_valid():
            user_personal_data_form.save()
        else:
            message = user_personal_data_form.non_field_errors
    if user_object:
        if not user_object.notifications_on:
            context['notifications_true'] = ""
            context['notifications_false'] = "checked"
    return render(request, 'profile/profile.html', {'data': user_object, 'disabled': disabled, 'message': message,
                                                    'country_help_text': 'Select the country for tracking',
                                                    'context': context})


@login_required
@otp_required(login_url='/otp/')
def deactivate_account(request):
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    message = ""
    if request.method == 'POST':
        if request.POST.get('confirm') == "YES":
            current_user = request.user
            current_user.is_active = False
            current_user.save()
            return redirect('/')
        else:
            message = "Please type YES"
    return render(request, 'profile/deactivate.html', {'message': message, 'context': context})


def basic_profile(request):
    if not request.user.first_name:
        return 0
    try:
        UserCountryData.objects.get(user=request.user)
    except ObjectDoesNotExist:
        return 0


@login_required
@otp_required(login_url='/otp/')
def travel_date_view(request, form_action='None', row_id='None'):
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    return_value = basic_profile(request)
    if return_value == 0:
        return redirect('/profile')
    data = {}
    data_list = UserCountryData.objects.get(user=request.user)
    user_country_code = data_list.country_name
    data['country'] = COUNTRIES[user_country_code]
    data['disabled'] = "disabled"
    data['context'] = context
    user_travel_dates = TravelData.objects.filter(user=request.user).order_by("-exit_date")
    data['form'] = TravelDatesForm()
    data['db_data'] = user_travel_dates
    if request.method == 'POST':
        check_form_origin = request.POST.get('form_type')
        if check_form_origin == "delete":
            id_list_to_delete = request.POST.getlist('id_list')
            for tuple_id in id_list_to_delete:
                tuple_row = TravelData.objects.filter(user=request.user, pk=tuple_id)
                if tuple_row:
                    tuple_row.delete()
                    update_final_notifications(request)
                else:
                    security_breach_email(request)
        else:
            # Loading data to be editied.
            if form_action == "edit":
                data_found = TravelData.objects.filter(user=request.user, pk=row_id)
                if data_found:
                    user_travel_data = TravelData.objects.get(user=request.user, pk=row_id)
                    temp_form = TravelDatesForm(instance=user_travel_data)
                    data['edit'] = 1
                    data['form'] = temp_form
                    data['edit_id'] = user_travel_data.id
                else:
                    security_breach_email(request)
            else:
                if request.POST.get('edit_id'):
                    # Editing existing data
                    edit_id = request.POST.get('edit_id')
                    try:
                        data_found = TravelData.objects.get(user=request.user, pk=edit_id)
                        form = TravelDatesForm(request.POST, instance=data_found)
                    except ObjectDoesNotExist:
                        security_breach_email(request)
                        form = TravelDatesForm(request.POST)
                        form.add_error(None, "Security Breach Reported!!")
                else:
                    # For saving new data
                    form = TravelDatesForm(request.POST)
                if form.is_valid():
                    temp_form = form.save(commit=False)
                    date_format = "%Y-%m-%d"
                    entry_date = request.POST.get('entry_date')
                    exit_date = request.POST.get('exit_date')
                    number_of_days = (datetime.strptime(entry_date, date_format).date() - datetime.strptime(exit_date,
                                                                                                            date_format).date()).days
                    temp_form.country_name = user_country_code
                    temp_form.user = request.user
                    if number_of_days > 0:
                        number_of_days -= 1
                    temp_form.number_of_days = number_of_days
                    temp_form.save()
                    update_final_notifications(request)
                else:
                    data['form'] = form

    return render(request, 'travel_data/travel_dates.html', data)


def user_date_data(request):
    data_list = UserCountryData.objects.get(user=request.user)
    user_country_code = data_list.country_name
    entry_date = data_list.entry_date
    pr_date = data_list.pr_date
    final_date_p = 1
    if not pr_date:
        pr_date = datetime.now().date()
        final_date_p = 0
    # todo
    # need to store target_days in db according to country
    target_days = 365 * 3
    five_years_back = (datetime.now().date() - timedelta(days=5 * 365))
    if entry_date < five_years_back:
        entry_date = five_years_back
    if pr_date < five_years_back:
        pr_date = five_years_back
    if entry_date == pr_date:
        total_days = (datetime.now().date() - entry_date).days
        total_days_out = TravelData.objects.filter(user=request.user, country_name=user_country_code).aggregate(
            Sum('number_of_days'))
        if total_days_out['number_of_days__sum']:
            out_days = total_days_out['number_of_days__sum']
        else:
            out_days = 0
        accountable_days = total_days - out_days
        days_left = target_days - accountable_days
        bar_graph_text = ""
        chart_labels = "['Elapsed Days', 'Accountable Days', 'Target Days' , 'Days Left' , 'Days Out']"
        chart_values = "[ '%d','%d','%d','%d','%d' ]" % (total_days, accountable_days, target_days, days_left, out_days)
    else:
        non_pr_total_days = (pr_date - entry_date).days
        pr_total_days = (datetime.now().date() - pr_date).days
        non_pr_out_days_data = TravelData.objects.filter(user=request.user, country_name=user_country_code,
                                                         exit_date__gte=entry_date, exit_date__lt=pr_date).aggregate(
            Sum('number_of_days'))
        if non_pr_out_days_data['number_of_days__sum']:
            non_pr_out_days = non_pr_out_days_data['number_of_days__sum']
        else:
            non_pr_out_days = 0
        pr_out_days_data = TravelData.objects.filter(user=request.user, country_name=user_country_code,
                                                     exit_date__gte=pr_date, entry_date__lte=datetime.now()).aggregate(
            Sum('number_of_days'))
        if pr_out_days_data['number_of_days__sum']:
            pr_out_days = pr_out_days_data['number_of_days__sum']
        else:
            pr_out_days = 0
        non_pr_accountable = math.floor((non_pr_total_days - non_pr_out_days) / 2)
        if non_pr_accountable > 365:
            non_pr_accountable = 365
        out_days = pr_out_days + math.floor((non_pr_out_days / 2))
        total_days = (datetime.now().date() - entry_date).days
        accountable_days = pr_total_days - pr_out_days + non_pr_accountable
        if accountable_days < 0:
            accountable_days = 0
        days_left = target_days - accountable_days
        chart_labels = "['Elapsed', 'Total Accountable', 'Target' , 'Left' , 'PR Out', " \
                       "'BPR out', 'BPR total', 'Total PR' ] "
        chart_values = "[ '%f','%f','%d','%f','%d','%f','%f','%d' ]" % (
            total_days, accountable_days, target_days, days_left, pr_out_days, math.floor(non_pr_out_days / 2),
            math.floor(non_pr_total_days / 2),
            pr_total_days)
        bar_graph_text = "BPR: Before Becoming Permanent Resident."
    percentage_complete = (accountable_days / target_days) * 100
    final_date = ""
    if final_date_p == 1:
        final_date = (datetime.now() + timedelta(days=days_left)).date()
    data = {
        'accountable_days': accountable_days,
        'country': COUNTRIES[user_country_code],
        'days_left': days_left,
        'entry_date': entry_date,
        'out_days': out_days,
        'percentage_complete': percentage_complete,
        'pr_date': pr_date,
        'target_days': target_days,
        'total_days': total_days,
        'chart_labels': chart_labels,
        'chart_values': chart_values,
        'bar_graph_text': bar_graph_text,
        'final_date': final_date
    }
    return data


@login_required
@otp_required(login_url='/otp/')
def dashboard_view(request):
    data = user_date_data(request)
    context = {
        'SCRIPT_VERSION': SCRIPT_VERSION
    }
    data['context'] = context
    return render(request, 'dashboard.html', data)


# Function to update date for full days completion
def update_final_notifications(request):
    data = user_date_data(request)
    final_date = data['final_date']
    # Check if person is PR and has a final date
    if final_date:
        user_data = PRNotifications.objects.filter(user=request.user, notification_type='final')
        if user_data:
            # Update query with date and bool changes as email might have been deleted or sent already.
            PRNotifications.objects.filter(user=request.user, notification_type='final').update(email_date=final_date,
                                                                                                email_sent=False,
                                                                                                email_deleted=False)
            # Update reminder date
            set_next_reminder(request)
        else:
            # creating first entry
            user_data = PRNotifications()
            user_data.user = request.user
            user_data.notification_type = "final"
            user_data.email_date = final_date
            user_data.email_sent = False
            user_data.save()
            # Update reminder date
            set_next_reminder(request)


# Function to send daily notifications
def send_email_notifications(request):
    ip = get_client_ip(request)
    if ip == '35.192.133.65':
        # Fetch rows less than equal to today
        data_list = PRNotifications.objects.filter(email_date__lte=datetime.now().date(), email_sent=False,
                                                   email_deleted=False)
        if data_list:
            # Get domain & protocol for email links
            pr_protocol = request.scheme
            domain = request.META['HTTP_HOST']
            # Parse through the return rows for reminder
            for row in data_list:
                user = row.user
                notifications_object = UserCountryData.objects.get(user=user)
                # Send email if user active and has notifications enabled else delete mail
                if user.is_active and notifications_object.notifications_on:
                    # Email type final reminder or regular reminder
                    if row.notification_type == "final":
                        mail_subject = "Congratulations you can apply for Citizenship [" + domain + "]"
                        mail_body = render_to_string('email/final_email.html', {
                            'user': user,
                            'domain': domain,
                            'protocol': pr_protocol,
                        })
                        send_mail(mail_subject, mail_body, EMAIL_HOST_USER, [user.email], fail_silently=False)
                        PRNotifications.objects.filter(id=row.id).update(email_sent=True)
                        PRNotifications.objects.filter(user=request.user, notification_type='reminder',
                                                       email_date__gte=datetime.now().date()).delete()
                        return HttpResponse('Final Email Sent')
                    else:
                        mail_subject = "Reminder to add travel data [" + domain + "]"
                        mail_body = render_to_string('email/reminder_email.html', {
                            'user': user,
                            'domain': domain,
                            'protocol': pr_protocol,
                        })
                        send_mail(mail_subject, mail_body, EMAIL_HOST_USER, [user.email], fail_silently=False)
                        PRNotifications.objects.filter(id=row.id).update(email_sent=True)
                        request.user = user
                        set_next_reminder(request)
                        return HttpResponse('Reminder Email Sent')
                else:
                    PRNotifications.objects.filter(id=row.id).update(email_deleted=True)
                    return HttpResponse('No mail sent -- deleted')
        else:
            return HttpResponse('No mail to send')
    else:
        return HttpResponse('Page not found')


# Function to set next reminder
def set_next_reminder(request):
    reminder_date = (datetime.now() + timedelta(days=90)).date()
    PRNotifications.objects.filter(user=request.user, notification_type='reminder',
                                   email_date__gte=reminder_date).delete()
    user_data = PRNotifications()
    user_data.user = request.user
    user_data.notification_type = "reminder"
    user_data.email_date = reminder_date
    user_data.email_sent = False
    user_data.save()
    return True


# Get clients IP address
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def security_breach_email(request):
    current_user = request.user
    mail_subject = "Security Breach - prdt.nihit.in"
    mail_body = render_to_string('security_breach_email.html', {
        'user': current_user,
        'path': request.path_info,
        'content': request.META
    })
    send_mail(mail_subject, mail_body, EMAIL_HOST_USER, [EMAIL_ADMIN], fail_silently=False)


@login_required
def otp_view(request):
    next_url = request.GET.get('next')
    message = ""
    error = ""
    if request.method == 'POST':
        user_device = EmailDevice.objects.get(user=request.user)
        token_count = user_device.throttling_failure_count
        valid_time_stamp = user_device.valid_until
        count = 5 - token_count
        if count <= 0:
            data = {
                'errors': "Max OTP attempts reached"
            }
            return render(request, 'registration/otp.html', data)
        if (valid_time_stamp + timedelta(seconds=1800)) < timezone.now():
            return HttpResponseRedirect('/otp.html')
        token = request.POST.get('otp')
        check_user = user_device.verify_token(token)
        if check_user:
            request.session['otp_device_id'] = user_device.persistent_id
            request.user.otp_device = user_device
            if next_url:
                return HttpResponseRedirect(next_url)
            else:
                return HttpResponseRedirect('home.html')
        else:
            data = {
                'errors': "Incorrect OTP. Attempt Left : " + str(count)
            }
            return render(request, 'registration/otp.html', data)
    else:
        count = 5
        try:
            user_device = EmailDevice.objects.get(user=request.user)
            count = 5 - user_device.throttling_failure_count
            vaild_time_stamp = user_device.valid_until

        except ObjectDoesNotExist:
            EmailDevice(user=request.user).generate_token()
            user_device = EmailDevice.objects.get(user=request.user)
        if count > 0:
            test = EmailDevice.generate_challenge(user_device)
            if test == "sent by email":
                message = "OTP sent to your registered email. Valid for "
        else:
            error = "Account Locked for 30 minutes"
        data = {
            'message': message,
            'errors': error
        }
        return render(request, 'registration/otp.html', data)
