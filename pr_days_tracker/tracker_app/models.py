from django.db import models
# from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField


# Create your models here.

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):

    username = None
    email = models.EmailField(_('email'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()


class UserCountryData(models.Model):

    entry_date = models.DateField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # country_name = models.CharField(max_length=5, help_text='Select Country to monitor')
    country_name = CountryField()
    reminder_interval = models.CharField(default='3 M', max_length=4)
    pr_date = models.DateField(blank=True, null=True)
    notifications_on = models.BooleanField(default=True)

    class Meta:
        unique_together = ['country_name', 'user']

    def __str__(self):
        return "%s" % self.user


class TravelData(models.Model):

    country_name = CountryField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    entry_date = models.DateField()
    exit_date = models.DateField()
    number_of_days = models.IntegerField()
    destination_country = CountryField(blank=True, null=True)
    reason = models.TextField(blank=True, null=True)

    class Meta:
        unique_together = [['country_name', 'user', 'exit_date']]

    # def __str__(self):
     #   return "%s %s %s" % (self.user, self.country_name, self.exit_date)


class TravelHistory(models.Model):

    # country_name = models.CharField(max_length=5, help_text='Select Country')
    country_name = CountryField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    entry_date = models.DateField()
    exit_date = models.DateField()

    class Meta:
        unique_together = [['country_name', 'user', 'entry_date']]

    def __str__(self):
        return "%s %s %s" % (self.user, self.country_name, self.entry_date)


class PRNotifications(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notification_type = models.TextField()
    email_date = models.DateField()
    email_sent = models.BooleanField(default=False)
    email_deleted = models.BooleanField(default=False)

    class Meta:
        unique_together = [['notification_type', 'user', 'email_date']]

    def __str__(self):
        return "%s %s %s %s" % (self.user, self.notification_type, self.email_date, self.email_sent)