from django import forms
from .models import User, UserCountryData, TravelData
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from django_countries.widgets import CountrySelectWidget
from django_countries.fields import CountryField
from django.utils.safestring import mark_safe
from django_otp.plugins.otp_email.models import EmailDevice


class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    password1 = None

    class Meta:
        model = User
        fields = ("email", "password2")

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.is_active = False
        user.set_password(self.cleaned_data["password2"])
        if commit:
            user.save()
            my_group = Group.objects.get(name='Public Users')
            my_group.user_set.add(user)
            opt_device = EmailDevice()
            opt_device.user = user
            opt_device.name = "Email"
            opt_device.email = user.email
            opt_device.save()
        return user


class UserCountryForm(forms.ModelForm):
    class Meta:
        model = UserCountryData
        fields = ('entry_date', 'country_name', 'pr_date', 'notifications_on')
        widgets = {
            'entry_date': forms.DateInput(attrs={'placeholder': 'dd-mm-yyyy', 'autocomplete': 'off'}),
            'pr_date': forms.DateInput(attrs={'placeholder': 'dd-mm-yyyy', 'autocomplete': 'off'}),
            'notifications_on': forms.RadioSelect(
                choices=[
                    ('1', 'On'),
                    ('0', 'OFF'),
                ]
            )
        }

    def clean(self):
        cleaned_data = super().clean()
        entry_date = cleaned_data.get('entry_date')
        pr_date = cleaned_data.get('pr_date')
        if pr_date:
            if entry_date > pr_date:
                self.add_error('pr_date', "Entry date should be less than or equal to PR date")


class UserBasicData(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class TravelDatesForm(forms.ModelForm):
    reason = forms.CharField(required=False, label='Reason: Max 400 Characters', max_length=400,
                             widget=forms.Textarea(attrs={"rows": 3}))
    destination_country = CountryField(blank_label="Select Country").formfield(required=False,
                                                                               label=mark_safe(
                                                                                   'Destination Country <a class="text-primary" data-toggle="tooltip" data-placement="top" title="If you visited more than one country during the same absence, list the first country in \'Destination\' and list the other countries in the \'Reason\' field."> <i class="fa fa-info-circle fa-1x"></i></a>'))
    exit_date = forms.DateField(label='From',
                                widget=forms.DateInput(attrs={'placeholder': 'dd-mm-yyyy', 'autocomplete': 'off'}))
    entry_date = forms.DateField(label='To',
                                 widget=forms.DateInput(attrs={'placeholder': 'dd-mm-yyyy', 'autocomplete': 'off'}))

    class Meta:
        model = TravelData
        fields = ('entry_date', 'exit_date', 'country_name', 'destination_country', 'reason')

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(TravelDatesForm, self).__init__(*args, **kwargs)
        self.fields['country_name'].required = False

    def clean(self):
        cleaned_data = super().clean()
        entry_date = cleaned_data.get('entry_date')
        exit_date = cleaned_data.get('exit_date')
        if entry_date < exit_date:
            # raise forms.ValidationError("Exit date should be less than or equal to entry date", code='invalid')
            self.add_error('entry_date', "Entry date should be greater than or equal to exit date")
