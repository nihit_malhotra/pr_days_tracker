// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var acc_days = $("#acc_days").val()
var days_left = $("#days_left").val()
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Accountable Days", "Days Left"],
    datasets: [{
      data: [acc_days, days_left],
      backgroundColor: ['#009e24', '#007bff'],
      hoverBackgroundColor: ['#1cc88a', '#17a2b8'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 50,
  },
});
