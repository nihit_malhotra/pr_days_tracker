$(document).ready(function() {
    $("#id_pr_date").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        todayBtn: true,
    });
    $("#id_entry_date").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        useCurrent: false,
        todayBtn: true,
    });
    $('[data-toggle="tooltip"]').tooltip();
});