$(document).ready(function() {
    $("#id_exit_date").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        todayBtn: true,
    });
    $("#id_entry_date").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        useCurrent: false,
        todayBtn: true,
    });
});
function select_all(object) {
    $('#ptd_table input:checkbox').prop('checked',object.checked);
}
function delete_rows() {
var confirm = window.confirm("All selected entries will be deleted");
if( confirm == true) {
    $("#form_id_list").remove();
    var newForm = $('<form>',{
        'method': 'post',
        'id': 'form_id_list',
        'name': 'delete_data'
        });
        var id = "";
        var token = $("input[name=csrfmiddlewaretoken]").val();
        newForm.append(
            $('<input>', { 'name': 'csrfmiddlewaretoken',
                'value': token,
                'type': 'hidden'
                }
            )
        );
        newForm.append(
            $('<input>', { 'name': 'form_type',
                'value': "delete",
                'type': 'hidden'
                }
            )
        );
        $.each($("#ptd_table input[name='id_list']:checked"), function(){
            id = $(this).val();
            newForm.append(
                $('<input>', { 'name': 'id_list',
                    'value': id,
                    'type': 'hidden'
                    }
                )
            );
        });
        newForm.appendTo('body').submit();
    }
};